package com.gc.mapreduce;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Created by Administrator on 2018/11/1.
 */
public class WordCountReduce extends Reducer<Text,IntWritable,Text,IntWritable> {
    int sum;
    IntWritable v = new IntWritable();

    @Override
    protected void reduce(Text key, Iterable<IntWritable> values, Context context) throws IOException, InterruptedException {
        sum = 0;
        System.out.println("###########key:"+key);
        System.out.println("###########values"+values);
        for(IntWritable count : values){
            sum+=count.get();
        }
        v.set(sum);
        context.write(key,v);
    }
}
