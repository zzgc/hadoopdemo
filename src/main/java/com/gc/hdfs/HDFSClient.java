package com.gc.hdfs;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.io.IOException;

/**
 * Created by Administrator on 2018/10/31.
 */
public class HDFSClient {


    FileSystem fs;
    @Before
    public void Conn(){
        System.setProperty("HADOOP_USER_NAME","root");
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS","hdfs://hadoop1:9000");
        try {
            fs = FileSystem.get(conf);
            System.out.println("连接成功！");
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
    @Test
    public void mkdir(){
        try {
            fs.mkdirs(new Path("/hello"));
            System.out.println("创建成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void delete(){
        try {
            fs.delete(new Path("/hello/output"),true);
            System.out.println("删除成功");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void put(){
        try {
            fs.copyFromLocalFile(new Path("E:/hello.txt"),new Path("/hello"));
            System.out.println("上传成功！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void get(){
        try {
            fs.copyToLocalFile(false,new Path("/hello/hello.txt"),new Path("E:/hello.txt"),true);
            System.out.println("下载成功！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @Test
    public void rename(){
        try {
            fs.rename(new Path("/user/hello.txt"),new Path("/user/hello_r.txt"));
            System.out.println("重命名成功！");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void filelist(){
        System.out.println("获取文件详情");
        try {
            RemoteIterator<LocatedFileStatus> listfile = fs.listFiles(new Path("/"),true);
            while (listfile.hasNext()){
                LocatedFileStatus status = listfile.next();
                System.out.println("文件名："+status.getPath().getName());
                System.out.println("文件长度："+status.getLen());
                System.out.println("文件权限："+status.getPermission());
                System.out.println("文件所属分组："+status.getGroup());
                BlockLocation[] blockLocations = status.getBlockLocations();
                for (BlockLocation blockLocation:blockLocations){
                    String[] hosts = blockLocation.getHosts();
                    System.out.print("文件存储在：");
                    for(String host : hosts){
                        System.out.print(host+"  ");
                    }
                }
                System.out.println("------------------");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    @After
    public void close(){
        try {
            fs.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}


